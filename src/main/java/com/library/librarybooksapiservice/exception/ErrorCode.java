package com.library.librarybooksapiservice.exception;

public enum ErrorCode {

    BOOK_EXISTS("Book you are trying to insert already exists"),
    PROCESSING_FAILURE("Error during JSON data processing");


    final String message;

    ErrorCode(String message) {
        this.message = message;
    }
}
