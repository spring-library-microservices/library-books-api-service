package com.library.librarybooksapiservice.exception;

public class LibraryApiException extends RuntimeException{

    public LibraryApiException(ErrorCode errorCode) {
        super(errorCode.message);
    }
}
