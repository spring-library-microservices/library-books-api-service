package com.library.librarybooksapiservice.mapper;

import com.library.librarybooksapiservice.model.Author;
import org.springframework.stereotype.Component;

@Component
public class AuthorMapper {

    public Author toDao(String authorJson) {

        Author author = new Author();
        author.setName(authorJson.split(" ", 2)[0]);
        author.setSurname(authorJson.split(" ", 2)[1]);

        return author;
    }


}
