package com.library.librarybooksapiservice.mapper;

import com.library.librarybooksapiservice.dto.BookResponse;
import com.library.librarybooksapiservice.model.Author;
import com.library.librarybooksapiservice.model.Book;
import com.library.librarybooksapiservice.model.Category;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class BookMapper {

    public Book toDao(BookResponse bookResponse, Set<Category> categories, Set<Author> authors) {

        Book book = new Book();
        book.setTitle(bookResponse.getTitle());
        book.setCategories(categories);
        book.setBorrowingDate(null);
        book.setAuthors(authors);
        book.setAvailable(bookResponse.getAvailable());

        return book;
    }
}
