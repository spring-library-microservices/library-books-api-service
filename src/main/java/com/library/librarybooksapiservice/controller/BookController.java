package com.library.librarybooksapiservice.controller;

import com.library.librarybooksapiservice.dto.BookResponse;
import com.library.librarybooksapiservice.service.BookService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/google/books")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "/external/{isbn}")
    public BookResponse getBookByIsbnExternal(@PathVariable("isbn") String isbn) {
        return bookService.getBookByIsbnExternal(isbn);
    }
}
