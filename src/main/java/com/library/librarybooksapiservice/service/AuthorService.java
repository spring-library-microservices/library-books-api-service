package com.library.librarybooksapiservice.service;


import com.library.librarybooksapiservice.model.Author;
import com.library.librarybooksapiservice.repository.AuthorRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Service
public class AuthorService {

    private final AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public Set<Author> authorsToBeSaved(Set<Author> authors) {

        Set<Author> newAuthors = new HashSet<>();

        authors.forEach(author -> {
            Author foundAuthorByNameAndSurname = authorRepository
                    .findAuthorByNameAndSurname(author.getName(), author.getSurname());

            if (Objects.isNull(foundAuthorByNameAndSurname)) {

                foundAuthorByNameAndSurname = new Author();
                foundAuthorByNameAndSurname.setName(author.getName());
                foundAuthorByNameAndSurname.setSurname(author.getSurname());

            }
            newAuthors.add(foundAuthorByNameAndSurname);

        });

        return newAuthors;
    }
}
