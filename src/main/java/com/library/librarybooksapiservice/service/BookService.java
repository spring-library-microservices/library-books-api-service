package com.library.librarybooksapiservice.service;

import com.library.librarybooksapiservice.service.googlebooks.GoogleBooksApiClient;
import com.library.librarybooksapiservice.dto.BookResponse;
import com.library.librarybooksapiservice.exception.ErrorCode;
import com.library.librarybooksapiservice.exception.LibraryApiException;
import com.library.librarybooksapiservice.mapper.BookMapper;
import com.library.librarybooksapiservice.model.Book;
import com.library.librarybooksapiservice.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.Objects;


@Service
public class BookService {

    private final GoogleBooksApiClient googleBooksApiClient;
    private final BookRepository bookRepository;
    private final BookMapper bookMapper;
    private final AuthorService authorService;
    private final CategoryService categoryService;


    public BookService(GoogleBooksApiClient googleBooksApiClient, BookRepository bookRepository,
                       AuthorService authorService, BookMapper bookMapper, CategoryService categoryService) {
        this.googleBooksApiClient = googleBooksApiClient;
        this.bookRepository = bookRepository;
        this.authorService = authorService;
        this.bookMapper = bookMapper;
        this.categoryService = categoryService;
    }

    public BookResponse getBookByIsbnExternal(String isbn) {
        BookResponse bookApiResponse = googleBooksApiClient.getBooksInfoByIsbn(isbn);

        Book foundBook = bookRepository
                .findBookByTitle(bookApiResponse.getTitle());

        if (Objects.nonNull(foundBook)) {
            throw new LibraryApiException(ErrorCode.BOOK_EXISTS);
        }

        Book bookToBeSaved = bookMapper
                .toDao(bookApiResponse,
                        categoryService.categoriesToBeSaved(bookApiResponse.getCategories()),
                        authorService.authorsToBeSaved(bookApiResponse.getAuthors()));

        bookRepository.save(bookToBeSaved);

        return bookApiResponse;
    }


}



