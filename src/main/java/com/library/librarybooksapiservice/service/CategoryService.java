package com.library.librarybooksapiservice.service;

import com.library.librarybooksapiservice.model.Category;
import com.library.librarybooksapiservice.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Set<Category> categoriesToBeSaved(Set<String> categories) {

        Set<Category> newCategories = new HashSet<>();

        categories.forEach(category -> {

            Category foundCategoryByType = categoryRepository.findCategoryByType(category);

            if (Objects.isNull(foundCategoryByType)) {

                foundCategoryByType = new Category();
                foundCategoryByType.setType(category);
            }

            newCategories.add(foundCategoryByType);
        });

        return newCategories;
    }

}
