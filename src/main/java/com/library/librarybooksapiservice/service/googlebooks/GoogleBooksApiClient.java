package com.library.librarybooksapiservice.service.googlebooks;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.library.librarybooksapiservice.dto.BookResponse;
import com.library.librarybooksapiservice.exception.ErrorCode;
import com.library.librarybooksapiservice.exception.LibraryApiException;
import com.library.librarybooksapiservice.mapper.AuthorMapper;
import com.library.librarybooksapiservice.model.Author;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


@Component
public class GoogleBooksApiClient {

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final AuthorMapper authorMapper;
    private final static String GOOGLE_BOOKS_API_URL = "https://www.googleapis.com/books/v1/";
    private final static String BOOK_ISBN_URL = "%svolumes?q=isbn:%s";

    private GoogleBooksApiClient(
            RestTemplate restTemplate, ObjectMapper objectMapper,
            AuthorMapper authorMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
        this.authorMapper = authorMapper;
    }

    public BookResponse getBooksInfoByIsbn(String isbn) {

        String url = String.format(BOOK_ISBN_URL, GOOGLE_BOOKS_API_URL, isbn);

        ResponseEntity<String> exchange = restTemplate.exchange(
                url,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                String.class
        );

        BookResponse bookResponse;

        try {
            JsonNode node = objectMapper.readTree(exchange.getBody());

            String title = node.findValue("title").asText();

            Set<Author> authorsSet = getAuthorSet(node);

            Set<String> categoriesSet = getCategoriesSet(node);

            bookResponse = new BookResponse.Builder()
                    .setTitle(title)
                    .setAuthors(authorsSet)
                    .setCategories(categoriesSet)
                    .setIsAvailable(true)
                    .build();

        } catch (JsonProcessingException exception) {

            throw new LibraryApiException(ErrorCode.PROCESSING_FAILURE);

        }

        return bookResponse;
    }

    private Set<String> getCategoriesSet(JsonNode node) {
        Set<String> categories = new HashSet<>();
        ArrayNode categoriesNode = (ArrayNode) node.findValue("categories");
        Iterator<JsonNode> categoriesIterator = categoriesNode.elements();
        while (categoriesIterator.hasNext()) {
            JsonNode categoryNode = categoriesIterator.next();
            categories.add(categoryNode.asText());
        }
        return categories;
    }

    private Set<Author> getAuthorSet(JsonNode node) {
        Set<Author> authors = new HashSet<>();
        ArrayNode authorsNode = (ArrayNode) node.findValue("authors");
        Iterator<JsonNode> authorsIterator = authorsNode.elements();
        while (authorsIterator.hasNext()) {
            JsonNode authorNode = authorsIterator.next();
            authors.add(authorMapper.toDao(authorNode.asText()));
        }
        return authors;
    }


}
