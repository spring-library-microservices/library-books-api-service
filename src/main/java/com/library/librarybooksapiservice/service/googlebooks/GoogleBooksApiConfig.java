package com.library.librarybooksapiservice.service.googlebooks;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class GoogleBooksApiConfig {

    @Bean
    @Qualifier("googleBooksApiRestTemplate")
    public RestTemplate googleBooksApiRestTemplate(RestTemplateBuilder restTemplateBuilder){
        return restTemplateBuilder.build();
    }
}
