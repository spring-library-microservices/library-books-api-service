package com.library.librarybooksapiservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class LibraryBooksApiServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryBooksApiServiceApplication.class, args);
	}

}
