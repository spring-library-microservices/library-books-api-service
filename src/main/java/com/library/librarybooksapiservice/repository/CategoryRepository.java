package com.library.librarybooksapiservice.repository;


import com.library.librarybooksapiservice.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {


    @Query(value = "select * from categories c where c.type=:cat", nativeQuery = true)
    Category findCategoryByType(@Param("cat") String category);
}
