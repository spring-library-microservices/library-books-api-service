package com.library.librarybooksapiservice.repository;

import com.library.librarybooksapiservice.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AuthorRepository extends JpaRepository<Author,Long> {


    @Query(value = "select * from authors a where a.name=:name and a.surname=:surname", nativeQuery = true)
    Author findAuthorByNameAndSurname(@Param("name")String name,
                                      @Param("surname")String surname);

}
