package com.library.librarybooksapiservice.repository;
import com.library.librarybooksapiservice.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book,Long> {

    @Query(value = "SELECT * FROM books WHERE title=:title",nativeQuery = true)
    Book findBookByTitle(@Param("title")String title);
}
