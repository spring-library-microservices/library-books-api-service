package com.library.librarybooksapiservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.library.librarybooksapiservice.model.Author;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BookResponse {


    private String title;
    private boolean isAvailable;
    private Set<Author> authors;
    private Set<String> categories;

    public String getTitle() {
        return title;
    }

    public boolean getAvailable() {
        return isAvailable;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public Set<String> getCategories() {
        return categories;
    }


    private BookResponse(String title,
                         boolean isAvailable,
                         Set<Author> authors,
                         Set<String> categories)
                         {
        this.title = title;
        this.isAvailable = isAvailable;
        this.authors = authors;
        this.categories = categories;
    }

    public static class Builder {

        private String title;
        private boolean isAvailable;
        private Set<Author> authors;
        private Set<String> categories;


        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setIsAvailable(boolean isAvailable) {
            this.isAvailable = isAvailable;
            return this;
        }

        public Builder setAuthors(Set<Author> authors) {
            this.authors = authors;
            return this;
        }

        public Builder setCategories(Set<String> categories) {
            this.categories = categories;
            return this;
        }


        public BookResponse build() {
            return new BookResponse(title, isAvailable, authors,categories);
        }
    }
}
